# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20180219180040) do

  create_table "empleados", force: true do |t|
    t.integer  "sucursal_id"
    t.string   "nombre"
    t.string   "rfc"
    t.string   "puesto"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "empleados", ["sucursal_id"], name: "index_empleados_on_sucursal_id", using: :btree

  create_table "empresas", force: true do |t|
    t.string   "nombre_persona"
    t.string   "correo"
    t.string   "rfc"
    t.string   "nombre_empresa"
    t.string   "password_digest"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "sucursals", force: true do |t|
    t.string   "nombre"
    t.string   "calle"
    t.string   "colonia"
    t.string   "numero_ext"
    t.string   "numero_int"
    t.string   "codigo_postal"
    t.string   "ciudad"
    t.string   "pais"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "empresa_id"
  end

end
