class CreateEmpresas < ActiveRecord::Migration
  def change
    create_table :empresas do |t|
      t.string :nombre_persona
      t.string :correo
      t.string :rfc
      t.string :nombre_empresa
      t.string :password_digest

      t.timestamps
    end
  end
end
