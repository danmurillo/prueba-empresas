class CreateEmpleados < ActiveRecord::Migration
  def change
    create_table :empleados do |t|
      t.integer :sucursal_id
      t.string :nombre
      t.string :rfc
      t.string :puesto

      t.timestamps
    end
    add_index :empleados, :sucursal_id
  end
end
