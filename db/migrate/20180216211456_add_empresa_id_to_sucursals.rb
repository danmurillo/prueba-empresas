class AddEmpresaIdToSucursals < ActiveRecord::Migration
  def change
    add_column :sucursals, :empresa_id, :integer
  end
end
