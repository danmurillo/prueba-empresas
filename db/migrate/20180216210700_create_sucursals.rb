class CreateSucursals < ActiveRecord::Migration
  def change
    create_table :sucursals do |t|
      t.string :nombre
      t.string :calle
      t.string :colonia
      t.string :numero_ext
      t.string :numero_int
      t.string :codigo_postal
      t.string :ciudad
      t.string :pais

      t.timestamps
    end
  end
end
