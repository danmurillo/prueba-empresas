require 'test_helper'

class EmpresasControllerTest < ActionController::TestCase
  test "get signup" do
    get :new
    assert_response :success
  end

  test "get empresa_index redirect to session new" do
    get :show
    assert_redirected_to("/login")
  end

end
