require 'test_helper'

class EmpresaSignupTest < ActionDispatch::IntegrationTest
  def setup
		@empresa = empresas(:AwesomeCo)	
	end

  test "missing fields in empresa signup" do
  	get signup_path
  	assert_template "empresas/new"
  	post signup_path, {empresa: { rfc: "MUED941231LG0"} }
  	assert_template "empresas/new"
  end

  test "successful empresa signup" do
  	get signup_path
  	assert_template "empresas/new"
  	post signup_path, {empresa: { nombre_persona: @empresa.nombre_persona,
  	correo: @empresa.correo, rfc: @empresa.rfc, nombre_empresa: @empresa.nombre_empresa,
  	password: '123123', password_confirmation: '123123'  } }

  	assert_redirected_to login_path
  	follow_redirect!
    assert_template "sessions/new"
	end
end
