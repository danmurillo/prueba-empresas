require 'test_helper'

class LoggedSucursalTest < ActionDispatch::IntegrationTest
	def setup
		@empresa = empresas(:AwesomeCo)
		@sucursal = nil
		@empleado = nil
		get login_path
		post login_path, { correo: @empresa.correo, password: "123123" }
		follow_redirect!
	end

	test "sin sucursales en root" do
    get login_path
    post login_path, { correo: @empresa.correo, password: "123123" }
    follow_redirect!
    assert_select "h1", "No hay sucursales registradas para esta empresa"
  end

  test "registrar sucursal falla" do
    get new_sucursal_path
    assert_template "sucursals/new"
    post sucursals_path, { sucursal: { ciudad: "Mexicali" } }
    assert_template "sucursals/new"
    get root_path
    assert_template "empresas/show"
    assert_select "h1", "No hay sucursales registradas para esta empresa"
  end

	test "registrar sucursal" do
		get new_sucursal_path
		assert_template "sucursals/new"
		post sucursals_path, { sucursal: { nombre: "Mexicali" } }
		assert_redirected_to root_path
		follow_redirect!
		assert_template "empresas/show"
		assert_select "h1", false
		assert_select "td", "Mexicali"
		assert_select "td", "0"
	end

	test "navegar a editar sucursal y editar empleado" do
		setup_sucursal_empleado
		get root_path
		assert_template "empresas/show"
		assert_select "a[href=?]", edit_sucursal_path(@sucursal.id)
		assert_select "a[href=?][data-method=\"delete\"]", sucursal_path(@sucursal.id)
		
		get edit_sucursal_path(@sucursal.id)
		assert_template "sucursals/edit"
		assert_select "a[href=?]", edit_empleado_path(@empleado.id)
		assert_select "a[href=?][data-method=\"delete\"]", empleado_path(@empleado.id)

		get edit_empleado_path(@empleado.id)
		assert_template "empleados/edit"
	end

	test "editar empleado y editar sucursal" do
		setup_sucursal_empleado
		get root_path
		get edit_sucursal_path(@sucursal.id)
		get edit_empleado_path(@empleado.id)
		patch empleado_url(@empleado.id), { empleado: { nombre: "Juan " } }
		assert_redirected_to edit_sucursal_path(@sucursal.id)
		follow_redirect!
		assert_template "sucursals/edit"
		assert_select "td", "Juan"

		patch sucursal_url(@sucursal.id), { sucursal: { nombre: "Lazaro Cardenas" } }
		assert_redirected_to root_path
		follow_redirect!
		assert_template "empresas/show"
		assert_select "td", "Lazaro Cardenas"
	end

	test "editar empleado fail" do
		setup_sucursal_empleado
		get root_path
		get edit_sucursal_path(@sucursal.id)
		get edit_empleado_path(@empleado.id)
		patch empleado_url(@empleado.id), { empleado: { nombre: "" } }
		assert_template "empleados/edit"
	end

	test "editar sucursal fail" do
		setup_sucursal_empleado
		get root_path
		get edit_sucursal_path(@sucursal.id)
		patch sucursal_url(@sucursal.id), { sucursal: { nombre: "" } }
		assert_template "sucursals/edit"
	end

	test "registrar empleado" do
		setup_sucursal_empleado
		get new_empleado_path
		assert_template "empleados/new"
		post empleados_path, { empleado: { nombre: "Eduardo Murillo", rfc: "MUED", sucursal_id: @sucursal.id } }
		assert_redirected_to edit_sucursal_path(@sucursal.id)
		follow_redirect!
		assert_template "sucursals/edit"
		assert_select "td", "Eduardo Murillo"
	end

	test "registrar empleado falla" do
		setup_sucursal_empleado
		get new_empleado_path
		assert_template "empleados/new"
		post empleados_path, { empleado: { nombre: "", rfc: "MUED", sucursal_id: @sucursal.id } }
		assert_template "empleados/new"
	end

	test "delete empleado" do
		setup_sucursal_empleado
		get root_path
		assert_select "td", "1"
		get edit_sucursal_path(@sucursal.id)
		assert_select "a[href=?][data-method=\"delete\"]", empleado_path(@empleado.id)
		delete empleado_path(@empleado.id)
		assert_select "a[data-method=\"delete\"]", false
		get root_path
		assert_select "td", "0"
	end

	test "delete sucursal" do
		setup_sucursal_empleado
		get root_path
		assert_template "empresas/show"
		assert_select "a[href=?]", edit_sucursal_path(@sucursal.id)
		delete sucursal_path(@sucursal.id)
		get root_path
		assert_select "a[href=?]", edit_sucursal_path(@sucursal.id), false
		assert_select "h1", "No hay sucursales registradas para esta empresa" 
	end

	test "new sucursal con empleado valido" do
		get new_sucursal_path
		assert_template "sucursals/new"
		post sucursals_path, { sucursal: { nombre: "Mexicali", empleados_attributes: {"0" => { nombre: "test", rfc: "AAA010101AAA", puesto: "tester"}} }  }
		assert_redirected_to root_path
		follow_redirect!
		assert_template "empresas/show"
		assert_select "h1", false
		assert_select "td", "Mexicali"
		assert_select "td", "1"
	end

	test "new sucursal con empleado invalido" do
		get new_sucursal_path
		assert_template "sucursals/new"
		post sucursals_path, { sucursal: { nombre: "Mexicali", empleados_attributes: {"0" => { nombre: "test", rfc: "", puesto: "tester"}} }  }
		assert_template "sucursals/new"
		assert_select "h1", "Registro de Sucursal"
	end

	private
		def setup_sucursal_empleado
			@sucursal = sucursals(:uno)
			@sucursal.empresa_id = @empresa.id
			@sucursal.save
			
			@empleado = empleados(:daniel)
			@empleado.sucursal_id = @sucursal.id
			@empleado.save
		end
end
