require 'test_helper'

class EmpresaLoginTest < ActionDispatch::IntegrationTest
	def setup
		@empresa = empresas(:AwesomeCo)	
	end

  test "login with invalid info" do
    get login_path
    assert_template "sessions/new"
    post login_path, { correo: "", password: "" }
    assert_match /Combinacion de correo\/password incorrectos/, response.body
    assert_template "sessions/new"
  end

  test "login with valid info followed by logout" do
  	get login_path
  	assert_template "sessions/new"
  	post login_path, { correo: @empresa.correo, password: "123123" }
    assert_redirected_to @empresa
    follow_redirect!
    assert_template "empresas/show"
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", login_path, count: 0
    assert_select "a[href=?]", logout_path
    assert_select "a[href=?]", new_sucursal_path
    assert_select "a[href=?]", new_empleado_path

    get logout_path
    assert_redirected_to login_path
    follow_redirect!
    assert_select "a[href=?]", root_path
    assert_select "a[href=?]", login_path
    assert_select "a[href=?]", new_empresa_path  
  end
end
