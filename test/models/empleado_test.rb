require 'test_helper'

class EmpleadoTest < ActiveSupport::TestCase
  def setup
  	@invalid_empleado = Empleado.new(nombre: "", puesto: "fail", rfc: "", sucursal_id: nil)
  	@valid_empleado = empleados(:daniel)
  	@sucursal = sucursals(:uno)
  	@valid_empleado.sucursal_id = @sucursal.id
  end

  test "empleado valido" do
  	assert @valid_empleado.valid?
  end

  test "empleado invalido"do
  	assert_not @invalid_empleado.valid?
  end

  test "guardar empleado valido" do
  	assert @valid_empleado.save
	end

	test "guardar empleado invalido fail" do
  	assert_not @invalid_empleado.save
	end

	test "delete empleado" do
		@valid_empleado.save
		id = @valid_empleado.id
		assert @valid_empleado.delete
		assert Empleado.find_by_id(id).nil?
	end
end
