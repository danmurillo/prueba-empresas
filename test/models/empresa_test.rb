require 'test_helper'

class EmpresaTest < ActiveSupport::TestCase
  def setup
  	@invalid_empresa = Empresa.new(correo: "", nombre_empresa: "Fail Co.")
  	@valid_empresa = empresas(:AwesomeCo)
  end

  test "empresa valida" do
  	assert @valid_empresa.valid?
  end

  test "empresa invalida"do
  	assert_not @invalid_empresa.valid?
  end

  test "guardar empresa valida" do
  	assert @valid_empresa.save
	end

	test "guardar empresa invalida fail" do
  	assert_not @invalid_empresa.save
	end

	test "delete empresa" do
		@valid_empresa.save
		id = @valid_empresa.id
		assert @valid_empresa.delete
		assert Empresa.find_by_id(id).nil?
	end
end
