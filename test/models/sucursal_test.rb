require 'test_helper'

class SucursalTest < ActiveSupport::TestCase
  def setup
  	@invalid_sucursal = Sucursal.new(nombre: "", codigo_postal: 12345, empresa_id: nil)
  	@valid_sucursal = sucursals(:uno)
  	@empresa = empresas(:AwesomeCo)
  	@valid_sucursal.empresa_id = @empresa.id
  end

  test "sucursal valida" do
  	assert @valid_sucursal.valid?
  end

  test "sucursal invalida"do
  	assert_not @invalid_sucursal.valid?
  end

  test "guardar sucursal valida" do
  	assert @valid_sucursal.save
	end

	test "guardar sucursal invalida fail" do
  	assert_not @invalid_sucursal.save
	end

	test "delete sucursal" do
		@valid_sucursal.save
		id = @valid_sucursal.id
		assert @valid_sucursal.delete
		assert Empleado.find_by_id(id).nil?
	end
end
