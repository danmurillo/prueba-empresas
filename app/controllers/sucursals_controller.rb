class SucursalsController < ApplicationController
	before_filter :authorize
	def new
		@sucursal = Sucursal.new
	end

	def create
		empleados = sucursal_params[:empleados_attributes]
		@sucursal = Sucursal.new(sucursal_params)
  	@sucursal[:empresa_id] = current_empresa.id
  	if @sucursal.save
	  	if empleados.present?
	  		empleados.each do |emp|
	  			#los atributos vienen en un arreglo ["0", {"nombre"=>"", "rfc"=>"", "puesto"=>""}]
		  		emp = emp.last
	  			if !emp.include?(:_destroy)
		  			emp[:sucursal_id] = @sucursal.id
	  				@empleado = Empleado.new(emp)
	  				if !@empleado.save
	  					render 'new' and return
	  				end
	  			end
	  		end
	  	end	  	
  		redirect_to root_path
  	else
  		render 'new'
  	end
	end

	def edit
		@sucursal = Sucursal.find(params[:id])
		@lista_empleados = @sucursal.listar_empleados
	end

	def update
		@sucursal = Sucursal.find(params[:id])
		if @sucursal.update(sucursal_params)
		  redirect_to root_path
		else
		  render 'edit'
		end
	end

	def destroy
		@sucursal = Sucursal.find(params[:id])
		@sucursal.destroy

		redirect_to root_path
	end

	private
		def sucursal_params
			params.require(:sucursal).permit(:nombre, :colonia, :numero_ext, :numero_int, :codigo_postal, :ciudad, :pais, empleados_attributes:[:nombre, :rfc, :puesto, :id, :_destroy])
			
		end
end
