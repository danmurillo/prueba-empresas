class SessionsController < ApplicationController
	def new
	end

	def create
		empresa = Empresa.find_by_correo(params[:correo])
		if empresa && empresa.authenticate(params[:password])
			session[:empresa_id] = empresa.id
			redirect_to empresa
		else
			@error = "Combinacion de correo/password incorrectos"
			render 'new'
		end
	end

	def destroy
		session[:empresa_id] = nil
		redirect_to '/login'
	end
end
