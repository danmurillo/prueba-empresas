class EmpleadosController < ApplicationController
  before_filter :authorize
  def new
  	@empleado = Empleado.new
  	@lista_sucursales = current_empresa.listar_sucursales
  end

  def create
  	@empleado = Empleado.new(empleado_params)
  	if @empleado.save
  		redirect_to edit_sucursal_path(@empleado.sucursal_id)
  	else
  		@empleado = Empleado.new
  		@lista_sucursales = current_empresa.listar_sucursales
  		render 'new'
  	end
  end

  def edit
  	@empleado = Empleado.find(params[:id])
  	@lista_sucursales = current_empresa.listar_sucursales
  end

  def update
	  @empleado = Empleado.find(params[:id])
	 
	  if @empleado.update(empleado_params)
	    redirect_to edit_sucursal_path(Sucursal.find(@empleado.sucursal_id))
	  else
	  	@empleado = Empleado.find(params[:id])
	  	@lista_sucursales = current_empresa.listar_sucursales
	    render 'edit'
	  end
	end

	def destroy
		@empleado = Empleado.find(params[:id])
		sucursal = Sucursal.find(@empleado.sucursal_id)
		@empleado.destroy

		redirect_to edit_sucursal_path(sucursal)
	end

	private
		def empleado_params
		  params.require(:empleado).permit(:nombre, :rfc, :puesto, :sucursal_id)
		end
end
