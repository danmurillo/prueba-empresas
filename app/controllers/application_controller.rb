class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  def current_empresa
  	@current_empresa ||= Empresa.find(session[:empresa_id]) if session[:empresa_id]
  end
  helper_method :current_empresa

  def authorize
  	redirect_to '/login' unless current_empresa
  end

end
