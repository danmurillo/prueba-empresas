class EmpresasController < ApplicationController
  before_filter :authorize, only: [:show]
  def new
  	@empresa = Empresa.new
  end

  def create
  	@empresa = Empresa.new(empresa_params)
  	if @empresa.save
  		redirect_to :controller => 'sessions', :action => 'new'
  	else
  		render 'new'
  	end
  end

  def show
    @lista_sucursales = current_empresa.listar_sucursales
  end

  private
  	def empresa_params
  		params.require(:empresa).permit(:nombre_persona, :correo, :rfc, :nombre_empresa, :password, :password_confirmation)  		
  	end
end
