# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
#include custom.js.coffee
EmpresasController = Paloma.controller('Empresas')
EmpresasController.prototype.new = ->
	$("form").submit (e) ->
		e.preventDefault()
		if validarCampos() && validaPassword()
			$(this).unbind('submit').submit()

	$("input").focus ->
		pintarCampo(this, '')

	$("input").focusout ->
		if $(this).val() == ""
			pintarCampo(this, 'red')

validaPassword = ->
	pass = $('#empresa_password')
	pass_conf = $('#empresa_password_confirmation')

	if(pass.val() != pass_conf.val() || pass.val() == "")
		error = "<p>Password y Password confirmation deben tener el mismo valor.</p>"
		pintarCampo(pass, 'red')
		pintarCampo(pass_conf, 'red')
		$("#mensajes").append(error)
		return false
	else
		pintarCampo(pass, '')
		pintarCampo(pass_conf, '')
		return true
