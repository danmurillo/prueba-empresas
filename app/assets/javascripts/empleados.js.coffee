# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

EmpleadosController = Paloma.controller('Empleados')
EmpleadosController.prototype.new = ->
	$("form").submit (e) ->
		e.preventDefault()
		if validarEmpleado()
			$(this).unbind('submit').submit()


EmpleadosController.prototype.edit = ->
	$("form").submit (e) ->	
		e.preventDefault()
		if validarEmpleado()
			$(this).unbind('submit').submit()
  	

validarEmpleado = ->
	errores = ""
	elem = $("#empleado_nombre")
	if elem.val() == ""
		pintarCampo(elem, 'red')
		errores += "<p>El campo Nombre es requerido.</p>"
	else
		pintarCampo(elem, '')

	elem = $("#empleado_rfc")
	if elem.val() == ""
		pintarCampo(elem, 'red')
		errores += "<p>El campo RFC es requerido.</p>"
	else
		pintarCampo(elem, '')
		
	$("#mensajes").html(errores)

	return errores == ""