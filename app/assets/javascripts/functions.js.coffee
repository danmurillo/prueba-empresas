@pintarCampo = (field, color) ->
	$(field).css({'border-color': color })

@validarCampos = ->
	campos = []
	$("input").each (i) ->
		if $(this).val() == ""
			pintarCampo(this, 'red')
			campos.push($(this).attr('id'))
		else
			pintarCampo(this, '')

	errores = ""
	for i in [0...campos.length]
		label = $("label[for='#{campos[i]}']").text();
		errores += "<p>#{label} es campo requerido<p>"

	$("#mensajes").html(errores)
	if errores == ""
		return true
	else
		return false