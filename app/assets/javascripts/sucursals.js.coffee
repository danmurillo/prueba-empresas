# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
SucursalsController = Paloma.controller('Sucursals')
SucursalsController.prototype.new = ->
	$("form").submit (e) ->
		e.preventDefault()
		sucursalValida = validarSucursal()
		empleadosValido = validarEmpleados()
		if sucursalValida && empleadosValido
			$(this).unbind('submit').submit()

	# limita a un máximo de 5 campos de empleados
	$(document).on "fields_added.nested_form_fields", (event, param) ->
  	total_formas = contarFormasEmpleado()
  	if total_formas	>= 5
  		$("#add-empleado").addClass("disabled")
	   
	$(document).on "fields_removed.nested_form_fields", (event, param) ->
   	total_formas = contarFormasEmpleado()
   	if total_formas < 5
    	$("#add-empleado").removeClass("disabled")

SucursalsController.prototype["edit"] = ->
	$("form").submit (e) ->
		e.preventDefault()
		if validarSucursal()
			$(this).unbind('submit').submit()

validarSucursal = ->
	errores = ""
	elem = $("#sucursal_nombre")
	if elem.val() == ""
		pintarCampo(elem, 'red')
		errores += "<p>El campo Nombre es requerido para la sucursal.</p>"
	else
		pintarCampo(elem, '')

	errores += validaNumerico("sucursal_numero_ext")
	errores += validaNumerico("sucursal_numero_int")
	errores += validaNumerico("sucursal_codigo_postal")
	$("#mensajes").html(errores)

	return errores == ""

validaNumerico = (campo) ->
	elem = $("##{campo}")
	if elem.val() != "" && !isNumber(elem.val()) 
		pintarCampo(elem, 'red')
		label = $("label[for='#{campo}']").text();
		return "<p>El campo #{label} debe ser un número válido.</p>"
	else
		pintarCampo(elem, '')
		return ""

isNumber = (num) ->
	return !isNaN(num) && num > 0

validarEmpleados = ->
	error_nombre = ""
	error_rfc = ""
	c_nom_validos = 0
	c_rfc_validos = 0
	nombres = $(".input-empleados-nombre")
	nombres.each (i) ->
		if $(this).parent().parent().css("display") != "none"
			if $(this).val() == ""
				pintarCampo(this, 'red')
				error_nombre = "<p>Nombre de empleado vacio no es permitido.</p>"
			else
				c_nom_validos++
				pintarCampo(this, '')

	rfcs = $(".input-empleados-rfc")
	rfcs.each (i) ->
		if $(this).parent().parent().css("display") != "none"
			if $(this).val() == ""
				pintarCampo(this, 'red')
				error_rfc = "<p>RFC de empleado vacio no es permitido.</p>"
			else
				c_rfc_validos++
				pintarCampo(this, '')

	total_formas = contarFormasEmpleado()
	if c_rfc_validos == c_nom_validos && c_nom_validos == total_formas
		return true
	else
		$("#mensajes").append(error_nombre + error_rfc)
		return false

contarFormasEmpleado = ->
	total_formas = 0
	$('fieldset').each (i) ->
		if $(this).css("display") != "none"
			total_formas++
	return total_formas
