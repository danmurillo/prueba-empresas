# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

SessionsController = Paloma.controller('Sessions')

SessionsController.prototype.new = ->
	$("form").submit (e) ->
		e.preventDefault()
		if validarCampos()
			$(this).unbind('submit').submit()

	$("input").focus ->
		pintarCampo(this, '')

	$("input").focusout ->
		if $(this).val() == ""
			pintarCampo(this, 'red')

validarCampos = ->
	campos = []
	$("input").each (i) ->
		if $(this).val() == ""
			pintarCampo(this, 'red')
			campos.push($(this).attr('id'))
		else
			pintarCampo(this, '')

	errores = ""
	for i in [0...campos.length]
		label = $("label[for='#{campos[i]}']").text();
		errores += "<p>#{label} es campo requerido<p>"

	$("#mensajes").html(errores)
	if errores == ""
		return true
	else
		return false