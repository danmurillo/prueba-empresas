class Empleado < ActiveRecord::Base
	validates :nombre, :rfc, :sucursal_id, :presence => true
	belongs_to :sucursal
end
