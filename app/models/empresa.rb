class Empresa < ActiveRecord::Base
	validates :nombre_persona, :correo, :rfc, :nombre_empresa, :password_digest, :presence => true
	has_many :sucursals
	has_secure_password

	def listar_sucursales
		Sucursal.where(empresa_id: self.id)
	end

	def Empresa.digest(string)
	  cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
	                                                BCrypt::Engine.cost
	  BCrypt::Password.create(string, cost: cost)
	end
end
