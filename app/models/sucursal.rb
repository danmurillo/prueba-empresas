class Sucursal < ActiveRecord::Base
	validates :nombre, :empresa_id, :presence => true
	belongs_to :empresa
	has_many :empleados
	accepts_nested_attributes_for :empleados, allow_destroy: true, reject_if: proc { |attributes| attributes[:nombre].blank? || attributes[:rfc].blank? || attributes[:empresa_id].blank? }

	def listar_empleados
		Empleado.where(sucursal_id: self.id)
	end
end
